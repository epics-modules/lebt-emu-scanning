/*
#  C.E.A. IRFU/SIS/LDISC
#
*/

program checkMTRBrake

%%#include <stdio.h>
%%#include <string.h>
%%#include <stdlib.h>

/**** MTR PV *******/
short ErrorBrakeMtrGet;
assign ErrorBrakeMtrGet to "{PMACPREFIX}:CURRENT_ERR_R";
monitor ErrorBrakeMtrGet;
evflag ErrorBrakeMtrEvent;
sync ErrorBrakeMtrGet ErrorBrakeMtrEvent;

short statusMTRMov;
assign statusMTRMov to "{PMACPREFIX}:{MOTOR_NAME}.MOVN";
monitor statusMTRMov;

short ErrorBrakeMtrSet;
assign ErrorBrakeMtrSet to "{PMACPREFIX}:CURRENT_ERR_S";
monitor ErrorBrakeMtrSet;

short brakeMtrCMD;
assign brakeMtrCMD to "{DEVICE}-PMAC:BRAKE_S";
monitor brakeMtrCMD;

short brakeMtrGET;
assign brakeMtrGET to "{DEVICE}-PMAC:BRAKE_R";
monitor brakeMtrGET;
evflag brakeMtrGETEvent;
sync brakeMtrGET brakeMtrGETEvent;

short AbortscanMtrCMD;
assign AbortscanMtrCMD to "{DEVICE}:MTR-ABORT.PROC";
monitor AbortscanMtrCMD;

short countErrors=0;

ss ss1
{
	state init {
	    	      when (delay(1.0) && (pvConnectCount() == pvChannelCount())) 
					{
					printf("\nWAIT\n");
					ErrorBrakeMtrSet=0;
			        pvPut(ErrorBrakeMtrSet);
					} state waiting
		   }
	
	state waiting
		   {    
		    when(efTestAndClear(ErrorBrakeMtrEvent)&&(ErrorBrakeMtrGet==1)&&(delay(0.1)))
			    {
			    printf("\nERRROR CURRENT : BRAKE OFF \n");
			    brakeMtrCMD=1;
			    pvPut(brakeMtrCMD);	
			    delay(1);
			    ErrorBrakeMtrSet=0;
			    pvPut(ErrorBrakeMtrSet);	   
			    }state count	    
		    }
		    
	state count	
	       {
	       when(countErrors>3)
			    {
			    printf("\nERRROR CURRENT : STOP MEASURE \n");
			    }state stopMeasure
			    
			when((delay(1.0)) && (statusMTRMov==1))
			    {
			    printf("\nERRROR CURRENT : PROBLEM SOLVED \n");
			    countErrors=0;
			    }state waiting
			    
			when((delay(1.0)) && (statusMTRMov==0))
			    {
			    countErrors=countErrors+1;
			    }state waiting
			}


	state stopMeasure
		 {	
			when(delay(1.0)){
			printf("\n ABORT\n");
			AbortscanMtrCMD=1;
			pvPut(AbortscanMtrCMD);
			countErrors=0;
			}state waiting
		 }

}

