include $(EPICS_ENV_PATH)/module.Makefile

DOC = doc/README.md
OPIS += opi/

USR_DEPENDENCIES = FastAcquisition,1.0.4-catania
USR_DEPENDENCIES += scanning,2.0.8+

vpath % ../../misc
